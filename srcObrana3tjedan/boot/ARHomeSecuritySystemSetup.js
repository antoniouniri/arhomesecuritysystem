
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'
import * as VueGoogleMaps from 'vue2-google-maps'

const firebaseConfig = {
  apiKey: 'AIzaSyAnhR8VIgwP3NaRKygWYV7esIihGRPCFYo',
  authDomain: 'arhomesecuritysystem.firebaseapp.com',
  databaseURL: 'https://arhomesecuritysystem-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'arhomesecuritysystem',
  storageBucket: 'arhomesecuritysystem.appspot.com',
  messagingSenderId: '59932595595',
  appId: '1:59932595595:web:e9164f1a083555a9f18b8e',
  measurementId: 'G-Q70Y2EDR30'
}

firebase.initializeApp(firebaseConfig)
export default ({
  Vue
}) => {
  Vue.prototype.$auth = firebase.auth()
  Vue.prototype.$db = firebase.firestore()
  Vue.prototype.$storage = firebase.storage()
  Vue.use(VueGoogleMaps, {
    load: {
      key: 'AIzaSyDdjSZ459FzeHtyeGDtON3wKe3lQwHHyOI'
    },
    installComponents: true
  })
}
