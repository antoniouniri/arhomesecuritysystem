const routes = [{
  path: '/Login',
  component: () => import('layouts/LoginPageLayout.vue'),
  children: [
    {
      path: '',
      component: () =>
        import('pages/Login/LoginIndex.vue')
    }
  ]
},
{
  path: '/',
  component: () => import('layouts/LoginPageLayout.vue'),
  children: [
    {
      path: '',
      component: () =>
        import('pages/Login/LoginIndex.vue')
    }
  ]
},
{
  path: '/Administration',
  component: () =>
    import('layouts/ARHomeSecuritySystemLayout.vue'),
  meta: { auth: true },
  children: [
    {
      path: '/Sensors',
      meta: { auth: true },
      component: () =>
        import('pages/ARHomeSecuritySystem/SensorsIndex.vue')
    },
    {
      path: '/SensorsOld',
      meta: { auth: true },
      component: () =>
        import('pages/ARHomeSecuritySystem/SensorsOld.vue')
    },
    {
      path: '/Modules',
      meta: { auth: true },
      component: () =>
        import('pages/ARHomeSecuritySystem/ModulesIndex.vue')
    },
    {
      path: '/ModulesOld',
      meta: { auth: true },
      component: () =>
        import('pages/ARHomeSecuritySystem/ModulesIndexOld.vue')
    },
    {
      path: '/Settings',
      meta: { auth: true },
      component: () =>
        import('pages/ARHomeSecuritySystem/SettingsIndex.vue')
    },
    {
      path: '/User',
      meta: { auth: true },
      component: () =>
        import('pages/ARHomeSecuritySystem/UserIndex.vue')
    },
    {
      path: '/Map',
      meta: {},
      component: () =>
        import('pages/Maps.vue')
    }
  ]
}
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}
export default routes
