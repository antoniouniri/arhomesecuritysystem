
const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var admin = require("firebase-admin");

var serviceAccount = require("./velerioimeteosystem-fb3b2-firebase-adminsdk-dhcev-5d3286de61.json");

var models = require ( './models.js' )

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();

app.get('/getmeteostation', (request, response) => {
    let res = [];
    db.collection('meteoStation')
        .get()
        .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                let document = {
                    id: doc.id,
                    data: doc.data()
                }
                res.push(document)
            })
            return response.send(res)
        })
        .catch ((error) => {
            return response.send("Error getting documents: ", error);
        })
})
//meteoStation
app.get('/hello', (request, response) => {
    return response.send('Hello world');
});

app.post('/addmeteostation', (request, response) => {
    const data = request.body;
    console.log(data.meteostation);
    return response.send('POST metoda -> Add '+data.meteostation);
});

app.put('/changemeteostation', (request, response) => {
    const data = request.body;
    console.log(data.meteostation);
    return response.send('PUT metoda -> Change '+data.meteostation);
})

app.delete('/delmeteostation', (request, response) => {
    const data = request.body;
    console.log('Delete '+data.meteostation);
    return response.send('Delete '+data.meteostation);
})

app.get('/sensor', ( request , response) => {
    let res = []
    if (typeof request.query.id === 'undefined') {
        db.collection ('sensor').get()
            .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    let document = {
                        id: doc.id ,
                        data: doc.data ()
                    }
                    res.push(document)
                })
                return response . send ( res )
            })
            .catch(function (error) {
                return response . send ( "Error getting docs: " + error )
            })
    } else {
        var docRef = db . collection ( 'sensor' ). doc ( request . query . id )
     docRef . get ()
        .then (( doc ) => {
            if ( typeof doc . data () !== 'undefined' ) {
                console . log ( 'doc' , doc . data ())
                let document = {
                    id: doc . id ,
                    data: doc . data ()
                }
                return response . send ( document )
    } else {
        return response . send (
            "Error getting document " +
            request . query . id +
            ": The document is undefined"
        )
    }
    })
    . catch ( function ( error ) {
        return response . send (
            "Error getting document " +
            request . query . id +
             ": " + error
        )
    })
    }
})
//1.a      +++++++++++++++++++++++++++++  
app . post ( '/meteostation' , ( request , response ) => {
    if ( Object . keys ( request . body ). length ) {
        db . collection ( 'meteostation' ). doc (). set ( request . body )
            . then ( function () {
                return response . send (
                    "Document successfully written - created!"
                )
        })
        . catch ( function ( error ) {
            return response . send (
                "Error writing document: " + error
        )
    })
    } else {
        return response . send (
            "No post data for new document. " +
            "A new document is not created!"
        )
        
    }
    })
//1.a
app . put ( '/meteostation' , ( request , response ) => {
    if ( Object . keys ( request . body ). length ) {
        if ( typeof request . query . id !== 'undefined' ) {
            db . collection ( 'meteostation' )
            . doc ( request . query . id )
            . set ( request . body )
            . then ( function () {
                return response . send (
                    "Document successfully written - " +
                    "updated!"
    )
    })
    . catch ( function ( error ) {
        return response . send (
            "Error writing document: " + error
            )
        })
        } else {
        return response . send (
                "A parameter id is not set. " +
                "A document is not updated!"
        )
        }
        } else {
        return response . send (
                "No post data for new document. " +
                "A document is not updated!"
            )
        }
        })
//1.a
app . delete ( '/meteostation' , ( request , response ) => {
    if ( typeof request . query . id !== 'undefined' ) {
        db . collection ( 'meteostation' ). doc ( request . query . id ). delete ()
        . then ( function () {
        return response . send (
            "Document successfully deleted!"
    )
    })
    . catch ( function ( error ) {
    return response . send (
        "Error removing document: " + error
    )
    })
    } else {
    return response . send (
        "A parameter id is not set. " +
        "A document is not deleted!"
    )
    }
    })

//meteoStationSensor +++++++++++++++++++++++

//1.a
app . post ( '/meteoStationSensor' , ( request , response ) => {
    if ( Object . keys ( request . body ). length ) {
        db . collection ( 'meteoStationSensor' ). doc (). set ( request . body )
            . then ( function () {
                return response . send (
                    "Document successfully written - created!"
                )
        })
        . catch ( function ( error ) {
            return response . send (
                "Error writing document: " + error
        )
    })
    } else {
        return response . send (
            "No post data for new document. " +
            "A new document is not created!"
        )
        
    }
    })
//1.a    
app . put ( '/meteoStationSensor' , ( request , response ) => {
    if ( Object . keys ( request . body ). length ) {
        if ( typeof request . query . id !== 'undefined' ) {
            db . collection ( 'meteoStationSensor' )
            . doc ( request . query . id )
            . set ( request . body )
            . then ( function () {
                return response . send (
                    "Document successfully written - " +
                    "updated!"
    )
    })
    . catch ( function ( error ) {
        return response . send (
            "Error writing document: " + error
            )
        })
        } else {
        return response . send (
                "A parameter id is not set. " +
                "A document is not updated!"
        )
        }
        } else {
        return response . send (
                "No post data for new document. " +
                "A document is not updated!"
            )
        }
        })
    
//1.a   
app . delete ( '/meteoStationSensor' , ( request , response ) => {
    if ( typeof request . query . id !== 'undefined' ) {
        db . collection ( 'meteoStationSensor' ). doc ( request . query . id ). delete ()
        . then ( function () {
        return response . send (
            "Document successfully deleted!"
    )
    })
    . catch ( function ( error ) {
    return response . send (
        "Error removing document: " + error
    )
    })
    } else {
    return response . send (
        "A parameter id is not set. " +
        "A document is not deleted!"
    )
    }
    })

//1.b +++++++++++++++++++++++++++++++
app.get('/meteoStationSensor', async (request, response) => {
    if (typeof request.query.id === 'undefined') {
        let res = []
        db.collection('meteoStationSensor').get()
        .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
            let document = {
                id: doc.id,
                data: doc.data()
            }
        res.push(document)
        })
    return response.send(res)
    })
    .catch(function (error) {
        return response.send(    
        )
    })
    } else {
    if (typeof request.query.subCollection === 'undefined') {
        let docRef = db
        .collection('meteoStationSensor')
        .doc(request.query.id)
        docRef.get()
        .then((doc) => {
        if (typeof doc.data() !== 'undefined') {
            let document = {
            id: doc.id,
            data: doc.data()
        }
    return response.send(document)
    } else {
        let error = (
        "Error getting document " +
        id +
        ": The document is undefined"
        )
    return response.send(error)
    }
    })
    .catch(function (error) {
    return response.send(error)
    })
    } else {
        let res = []
        let cRef = db
        .collection('meteoStationSensor')
        .doc(request.query.id)
        .collection(request.query.subCollection)
        cRef.get()
        .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
        let document = {
        id: doc.id,
        data: doc.data()
        }
        res.push(document)
        })
    return response.send(res)
    })
    .catch(function (error) {
    return response.send(
        "Error getting documents " +
        "of subcollection: " +
        error
        )
        })
        } }
    })


app.get('/meteoStation', async (request, response) => {
    let id = (
    typeof request.query.id !== 'undefined'
    ? request.query.id
    : null
    )
    let order = null
    let where = null
    if (id === null) {
    order = models.getOrder(request.query)
    where = models.getWhere(request.query)
    }
    models.get(db, 'meteoStation', id, order, where)
    .then(res => {
    return response.send(res)
    }).catch((error) => {
    return response.send(error)
    })
   })
   









app.listen(3000, () => {
    console.log("Server running on port 3000");
});
