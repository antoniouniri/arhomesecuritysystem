module.exports = {
    getOrder: function (requestQuery) {
        let order = {
        orderAttr: (
        typeof requestQuery.orderAttr !== 'undefined'
        ? requestQuery.orderAttr
        : null
        ),
        orderType: (
        typeof requestQuery.orderDesc !== 'undefined'
        ? 'desc'
        : 'asc'
        )
        }
        return order
        },
        getWhere: function (requestQuery) {
        let where = null
        if (
        typeof requestQuery.whereAttr !== 'undefined' &&
        typeof requestQuery.whereOp !== 'undefined' &&
        typeof requestQuery.whereVal !== 'undefined'
        ) {
        let op = '=='
        if (requestQuery.whereOp === 'eq') { op = '==' }
        else if (requestQuery.whereOp === 'gt') { op = '>' }
        else if (requestQuery.whereOp === 'ge') { op = '>=' }
        else if (requestQuery.whereOp === 'lt') { op = '<' }
        else if (requestQuery.whereOp === 'le') { op = '<=' }
        else if (requestQuery.whereOp === 'ne') { op = '!=' }
        else if (requestQuery.whereOp === 'array-contains') {
        op = 'array-contains'
        }
        else if (
        requestQuery.whereOp === 'array-contains-any'
        ) 
        op = 'array-contains-any'
        }
        else if (requestQuery.whereOp === 'in') { op = 'in' }
        else if (requestQuery.whereOp === 'not-in') {
        op = 'not-in'
        }
        where = {
        attr: requestQuery.whereAttr,
        op: op,
        val: requestQuery.whereVal
        }
        return where
        },
        




    getCollectionRef: function (db, collection, order, where) {
        var collectionRef = this.getCollectionRef(db, collection, order, where)

        console.log(where)
        if (order.orderAttr === null) {
        if (where && where.attr && where.op && where.val) {
        collectionRef = db.collection(collection)
        .where(where.attr, where.op, where.val)
        } else {
        collectionRef = db.collection(collection)
        }
        } else {
        if (where && where.attr && where.op && where.val) {
        collectionRef = db.collection(collection)
        .where(where.attr, where.op, where.val)
        .orderBy(order.orderAttr, order.orderType)
        } else {
        collectionRef = db.collection(collection)
        .orderBy(order.orderAttr, order.orderType)
        }
        }
        return collectionRef
        }

//3.4.4


}
    
    

  
    