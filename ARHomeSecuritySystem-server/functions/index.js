const express = require("express")
const bodyParser = require("body-parser")
const admin = require('firebase-admin')
const serviceAccount = require('./arhomesecuritysystem-firebase-adminsdk-s5o1g-0a357dc04d.json')

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
})

const db = admin.firestore()

const app = express()
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/setReading', (request, response) => {
    if (request.method === 'POST') {
        const data = request.body

        if (typeof data === 'undefined' || data === null ||
            typeof data.IDMeteoStation === 'undefined' || data.IDMeteoStation === null ||
            typeof data.IDSensor === 'undefined' || data.IDSensor === null ||
            typeof data.value === 'undefined' || data.value === null) {
            return response.status(400).send('The received data is not valid.')
        }
        const meteoStationCollection = db.collection('/HUB/LILrTQUpzv3aqUewoS5S/Module1')
        return meteoStationCollection.where('IDMeteoStation', '==', data.IDMeteoStation).get()
            .then(meteoStations => {
                if (meteoStations.empty) {
                    return response.status(404).send('There is no meteo staton with ID: ' + data.IDMeteoStation + '.')
                }
                const meteoStation = meteoStations.docs[0].data()
                const meteoStationSensorCollection = meteoStationCollection.doc(meteoStation.UIDMeteoStation).collection('Sensor')
                return meteoStationSensorCollection.where('IDSensor', '==', data.IDSensor).get()
                    .then(meteoStationSensors => {
                        if (meteoStationSensors.empty) {
                            return response.status(404).send('There is no sensor with ID: ' + data.IDSensor + ' on meteo staton with ID: ' + data.IDMeteoStation + '.')
                        }
                        const meteoStationSensor = meteoStationSensors.docs[0].data()
                        const reading = {
                            UIDReading: null,
                            UIDMeteoStation: meteoStation.UIDMeteoStation,
                            UIDMeteoStationSensor: meteoStationSensor.UIDMeteoStationSensor,
                            readingDateTime: new Date(),
                            value: data.value
                        }
                        return db.collection('reading').add(reading)
                            .then((doc) => {
                                var docRef = db.collection('reading').doc(doc.id)
                                return docRef.update({ UIDReading: doc.id })
                                    .then(() => {
                                        return response.status(200).send('OK.')
                                    })
                                    .catch((error) => {
                                        console.error(error)
                                        return response.status(500).send('Error updating UID in reading collection.')
                                    })
                            })
                            .catch((error) => {
                                console.error(error)
                                return response.status(500).send('Error adding new reading in reading collection.')
                            })
                    })
                    .catch(error => {
                        console.error(error)
                        return response.status(500).send('Error reading meteo station sensor collection.')
                    })
            })
            .catch(error => {
                console.error(error)
                return response.status(500).send('Error reading meteo station collection.')
            })
    }
    return response.status(403).send('Forbidden.')
})

app.post('/setReading2', (request, response) => {
    if (request.method === 'POST') {
        const data = request.body

        if (typeof data === 'undefined' || data === null ||
            typeof data.IDMeteoStation === 'undefined' || data.IDMeteoStation === null ||
            typeof data.IDSensor === 'undefined' || data.IDSensor === null ||
            typeof data.value === 'undefined' || data.value === null) {
            return response.status(400).send('The received data is not valid.')
        }
        const meteoStationCollection = db.collection('/HUB/LILrTQUpzv3aqUewoS5S/Module1')
        return meteoStationCollection.where('IDMeteoStation', '==', data.IDMeteoStation).get()
            .then(meteoStations => {
                if (meteoStations.empty) {
                    return response.status(404).send('There is no meteo staton with ID: ' + data.IDMeteoStation + '.')
                }
                const meteoStation = meteoStations.docs[0].data()
                const meteoStationSensorCollection = meteoStationCollection.doc(meteoStation.UIDMeteoStation).collection('Sensor')
                return meteoStationSensorCollection.where('IDSensor', '==', data.IDSensor).get()
                    .then(meteoStationSensors => {
                        if (meteoStationSensors.empty) {
                            return response.status(404).send('There is no sensor with ID: ' + data.IDSensor + ' on meteo staton with ID: ' + data.IDMeteoStation + '.')
                        }
                        const meteoStationSensor = meteoStationSensors.docs[0].data()
                        const reading = {
                            UIDReading: null,
                            UIDMeteoStation: meteoStation.UIDMeteoStation,
                            UIDMeteoStationSensor: meteoStationSensor.UIDMeteoStationSensor,
                            readingDateTime: new Date(),
                            value: data.value
                        }
                        return db.collection('reading2').add(reading)
                            .then((doc) => {
                                var docRef = db.collection('reading2').doc(doc.id)
                                return docRef.update({ UIDReading: doc.id })
                                    .then(() => {
                                        return response.status(200).send('OK.')
                                    })
                                    .catch((error) => {
                                        console.error(error)
                                        return response.status(500).send('Error updating UID in reading collection.')
                                    })
                            })
                            .catch((error) => {
                                console.error(error)
                                return response.status(500).send('Error adding new reading in reading collection.')
                            })
                    })
                    .catch(error => {
                        console.error(error)
                        return response.status(500).send('Error reading meteo station sensor collection.')
                    })
            })
            .catch(error => {
                console.error(error)
                return response.status(500).send('Error reading meteo station collection.')
            })
    }
    return response.status(403).send('Forbidden.')
})

app.get('/getReading', (request, response) => {
    let res = [];
    db.collection('reading')
        .get()
        .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                let document = {
                    id: doc.id,
                    data: doc.data()
                }
                res.push(document)
            })
            return response.send(res)
        })
        .catch((error) => {
            return response.send("Error getting documents: ", error);
        })
})

app.get('/getReading2', (request, response) => {
    let res = [];
    db.collection('reading2')
        .get()
        .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                let document = {
                    id: doc.id,
                    data: doc.data()
                }
                res.push(document)
            })
            return response.send(res)
        })
        .catch((error) => {
            return response.send("Error getting documents: ", error);
        })
})


app.put('/putReading/:id', (request, response) => {
    if (Object.keys(request.body).length) {
        if (typeof request.params.id !== 'undefined') {
            db.collection('reading')
                .doc(request.params.id)
                .update(request.body)
                .then(function () {
                    console.log("request.params.id -> ",request.params.id)
                    console.log("request.body -> ",request.body)
                    return response.send(
                        "Document successfully written - " +
                        "updated!"
                    )
                })
                .catch(function (error) {
                    return response.send(
                        "Error writing document: " + error
                    )
                })
        } else {
            return response.send(
                "A parameter id is not set. " +
                "A document is not updated!"
            )
        }
    } else {
        return response.send(
            "No post data for new document. " +
            "A document is not updated!"
        )
    }
})

app.put('/putReading2/:id', (request, response) => {
    if (Object.keys(request.body).length) {
        if (typeof request.params.id !== 'undefined') {
            db.collection('reading2')
                .doc(request.params.id)
                .update(request.body)
                .then(function () {
                    console.log("request.params.id -> ",request.params.id)
                    console.log("request.body -> ",request.body)
                    return response.send(
                        "Document successfully written - " +
                        "updated!"
                    )
                })
                .catch(function (error) {
                    return response.send(
                        "Error writing document: " + error
                    )
                })
        } else {
            return response.send(
                "A parameter id is not set. " +
                "A document is not updated!"
            )
        }
    } else {
        return response.send(
            "No post data for new document. " +
            "A document is not updated!"
        )
    }
})

// app.get('/ids/:id', (request, response) => {
//     console.log('Request Id:', request.params);
//     return response.send(request.params.id)
// })

app.delete('/delReading', (request, response) => {
    if (typeof request.body.id !== 'undefined') {
        db.collection('reading').doc(request.body.id).delete()
            .then(function () {
                return response.send(
                    "Document successfully deleted!"
                )
            })
            .catch(function (error) {
                return response.send(
                    "Error removing document: " + error
                )
            })
    } else {
        return response.send(
            "A parameter id is not set. " +
            "A document is not deleted!"
        )
    }
})

app.delete('/delReading2', (request, response) => {
    if (typeof request.body.id !== 'undefined') {
        db.collection('reading2').doc(request.body.id).delete()
            .then(function () {
                return response.send(
                    "Document successfully deleted!"
                )
            })
            .catch(function (error) {
                return response.send(
                    "Error removing document: " + error
                )
            })
    } else {
        return response.send(
            "A parameter id is not set. " +
            "A document is not deleted!"
        )
    }
})


app.listen(3000, () => {
    console.log("Server running on port 3000");
});
