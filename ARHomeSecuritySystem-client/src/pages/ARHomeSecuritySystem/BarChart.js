import { Bar } from 'vue-chartjs'
export default {
  watch: {
    update () {
      this.$data._chart.update()
    }
  },
  extends: Bar,
  props: ['chartData', 'update'],
  mounted () {
    this.renderChart(this.chartData, {
      maintainAspectRatio: false
    })
  }
}
